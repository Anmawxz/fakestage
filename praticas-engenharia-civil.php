<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/uikit.css">
    <link rel="stylesheet" href="css/main.css">

    <title>Práticas de Engenharia Civil</title>
</head>
<!--HEADER-->
<?php include_once('_include/header.php'); ?>
<body>
    <main>
        <h1 class="practicle-title uk-animation-slide-bottom-medium">Práticas de Engenharia Civil</h1>
        <hr class="uk-divider-icon">
        
        <!--Container to suport all course practices-->
        <div class="practicles-container row row-cols-1 row-cols-md-1 mb-1">
            <a class="block-link" href="contents/Práticas de Engenharia Civil/ensaio-de-granulometria/granulometria.php">
                <div class="block uk-card uk-animation-scale-up">
                    <img class="icon-practicles" src="icons/granulometria-icon.png" alt="">
                    <div class="card-title">Ensaio de granulometria</div>
                </div>
            </a>
            
        </div>
    </main>

</body>
<!--Footer and Scripts-->
<?php include_once('_include/footer.php'); ?>

</html>