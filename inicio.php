<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/uikit.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="shortcut icon" type="imagem/png" href="icons/icon.png">
    <title>Página Inicial</title>
</head>

<body>
    <!--HEADER-->
<?php include_once('_include/header.php'); ?>
    <main>
        <section class="wellcome-message">
           
            <div class="message-text uk-animation-slide-bottom-medium">
                <h3>Bem vinda, Heloísa Pimentel! </h3>
                <p >LIA é um aplicativo web de fácil e livre acesso que proporciona, em parâmetro virtual, ambientes nos quais serão simulados diversos ensaios e experimentos em vigor do ensino superior, visando maior praticidade no aprendizado. Explore e teste os laboratórios virtuais disponíveis.</p>
            </div>
            <img id="logo-larger" class="img-fluid" src="icons/logo-branca.png">
        </section>
    </main>
<!--Footer and Scripts-->
<?php include_once('_include/footer.php'); ?>
</body>




</html>